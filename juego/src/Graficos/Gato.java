package Graficos;
import Audio.Audio;

public class Gato extends javax.swing.JFrame 
{
    private boolean jugador;
    private String[][] movimientos;
    private Audio audio;
    
    public Gato() 
    {
        jugador = true; //se inicializa en el jujador 1 que son X
        movimientos = new String[3][3];
        llenarMatriz();
        audio = new Audio();
        initComponents();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        boton1 = new javax.swing.JButton();
        boton2 = new javax.swing.JButton();
        boton3 = new javax.swing.JButton();
        boton4 = new javax.swing.JButton();
        boton5 = new javax.swing.JButton();
        boton6 = new javax.swing.JButton();
        boton7 = new javax.swing.JButton();
        boton8 = new javax.swing.JButton();
        boton9 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Proyecto fianal I.S.C");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));

        boton1.setBackground(new java.awt.Color(153, 255, 255));
        boton1.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 48)); // NOI18N
        boton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton1ActionPerformed(evt);
            }
        });

        boton2.setBackground(new java.awt.Color(153, 255, 255));
        boton2.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 48)); // NOI18N
        boton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton2ActionPerformed(evt);
            }
        });

        boton3.setBackground(new java.awt.Color(153, 255, 255));
        boton3.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 48)); // NOI18N
        boton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton3ActionPerformed(evt);
            }
        });

        boton4.setBackground(new java.awt.Color(153, 255, 255));
        boton4.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 48)); // NOI18N
        boton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton4ActionPerformed(evt);
            }
        });

        boton5.setBackground(new java.awt.Color(153, 255, 255));
        boton5.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 48)); // NOI18N
        boton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton5ActionPerformed(evt);
            }
        });

        boton6.setBackground(new java.awt.Color(153, 255, 255));
        boton6.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 48)); // NOI18N
        boton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton6ActionPerformed(evt);
            }
        });

        boton7.setBackground(new java.awt.Color(153, 255, 255));
        boton7.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 48)); // NOI18N
        boton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton7ActionPerformed(evt);
            }
        });

        boton8.setBackground(new java.awt.Color(153, 255, 255));
        boton8.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 48)); // NOI18N
        boton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton8ActionPerformed(evt);
            }
        });

        boton9.setBackground(new java.awt.Color(153, 255, 255));
        boton9.setFont(new java.awt.Font("Tw Cen MT Condensed Extra Bold", 0, 48)); // NOI18N
        boton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton9ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(boton7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 104, Short.MAX_VALUE)
                    .addComponent(boton4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(boton1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(boton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(boton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(boton8, javax.swing.GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(boton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(boton6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(boton9, javax.swing.GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(boton3, javax.swing.GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE)
                    .addComponent(boton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(boton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(boton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(boton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(boton6, javax.swing.GroupLayout.DEFAULT_SIZE, 95, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(boton7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(boton8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(boton9, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)))
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        jPanel2.setBackground(new java.awt.Color(0, 255, 204));

        jLabel1.setFont(new java.awt.Font("Sakkal Majalla", 1, 24)); // NOI18N
        jLabel1.setText("Preyecto juego de gato");

        jLabel2.setFont(new java.awt.Font("Franklin Gothic Medium", 2, 12)); // NOI18N
        jLabel2.setText("Reiniciar juego");

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/reinten (1).png"))); // NOI18N
        jButton1.setText("Reiniciar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/salir (2).png"))); // NOI18N
        jButton2.setText("s a l i r");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/menu (1).png"))); // NOI18N
        jButton3.setText("Menu");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jLabel3.setText("Regresar al menu principal");

        jLabel4.setText("Salir del juego");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(137, 137, 137)
                .addComponent(jLabel4)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(133, 133, 133)
                        .addComponent(jLabel2))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(jLabel1))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel3)
                        .addGap(89, 89, 89))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(88, 88, 88)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButton1)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jButton3)
                                .addComponent(jButton2)))))
                .addContainerGap(64, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(23, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 0, 380, 315));

        pack();
    }// </editor-fold>                        

    private void boton1ActionPerformed(java.awt.event.ActionEvent evt) {                                       
       
       if(jugador && movimientos[0][0].equals("i"))
       {
           boton1.setText("X");
           setMovimiento(0,0,"X");
           jugador = false;
           imprimirMatriz();
           if(ganador1())
               javax.swing.JOptionPane.showMessageDialog(null, "El jugador 1 ha ganado!!");
       }
       else if(!jugador && movimientos[0][0].equals("i"))
       {
           boton1.setText("O");
           setMovimiento(0,0,"O");
           jugador = true;
           imprimirMatriz();
           if(ganador2())
               javax.swing.JOptionPane.showMessageDialog(null, "El jugador 2 ha ganado!!");
       }
    }                                      

    private void boton2ActionPerformed(java.awt.event.ActionEvent evt) {                                       
       if(jugador && movimientos[0][1].equals("i"))
       {
           boton2.setText("X");
           setMovimiento(0,1,"X");
           jugador = false;
           imprimirMatriz();
           if(ganador1())
               javax.swing.JOptionPane.showMessageDialog(null, "El jugador 1 ha ganado!!");
       }
       else if(!jugador && movimientos[0][1].equals("i"))
       {
           boton2.setText("O");
           setMovimiento(0,1,"O");
           jugador = true;
           imprimirMatriz();
           if(ganador2())
               javax.swing.JOptionPane.showMessageDialog(null, "El jugador 2 ha ganado!!");
       }
    }                                      

    private void boton3ActionPerformed(java.awt.event.ActionEvent evt) {                                       
       if(jugador && movimientos[0][2].equals("i"))
       {
           boton3.setText("X");
           setMovimiento(0,2,"X");
           jugador = false;
           imprimirMatriz();
           if(ganador1())
               javax.swing.JOptionPane.showMessageDialog(null, "El jugador 1 ha ganado!!");
       }
       else if(!jugador && movimientos[0][2].equals("i"))
       {
           boton3.setText("O");
           setMovimiento(0,2,"O");
           jugador = true;
           imprimirMatriz();
           if(ganador2())
               javax.swing.JOptionPane.showMessageDialog(null, "El jugador 2 ha ganado!!");
       }
    }                                      

    private void boton4ActionPerformed(java.awt.event.ActionEvent evt) {                                       
       if(jugador && movimientos[1][0].equals("i"))
       {
           boton4.setText("X");
           setMovimiento(1,0,"X");
           jugador = false;
           imprimirMatriz();
           if(ganador1())
               javax.swing.JOptionPane.showMessageDialog(null, "El jugador 1 ha ganado!!");
       }
       else if(!jugador && movimientos[1][0].equals("i"))
       {
           boton4.setText("O");
           setMovimiento(1,0,"O");
           jugador = true;
           imprimirMatriz();
           if(ganador2())
               javax.swing.JOptionPane.showMessageDialog(null, "El jugador 2 ha ganado!!");
       }
    }                                      

    private void boton5ActionPerformed(java.awt.event.ActionEvent evt) {                                       
       if(jugador && movimientos[1][1].equals("i"))
       {
           boton5.setText("X");
           setMovimiento(1,1,"X");
           jugador = false;
           imprimirMatriz();
           if(ganador1())
               javax.swing.JOptionPane.showMessageDialog(null, "El jugador 1 ha ganado!!");
       }
       else if(!jugador && movimientos[1][1].equals("i"))
       {
           boton5.setText("O");
           setMovimiento(1,1,"O");
           jugador = true;
           imprimirMatriz();
           if(ganador2())
               javax.swing.JOptionPane.showMessageDialog(null, "El jugador 2 ha ganado!!");
       }
    }                                      

    private void boton6ActionPerformed(java.awt.event.ActionEvent evt) {                                       
       if(jugador && movimientos[1][2].equals("i"))
       {
           boton6.setText("X");
           setMovimiento(1,2,"X");
           jugador = false;
           imprimirMatriz();
           if(ganador1())
               javax.swing.JOptionPane.showMessageDialog(null, "El jugador 1 ha ganado!!");
       }
       else if(!jugador && movimientos[1][2].equals("i"))
       {
           boton6.setText("O");
           setMovimiento(1,2,"O");
           jugador = true;
           imprimirMatriz();
           if(ganador2())
               javax.swing.JOptionPane.showMessageDialog(null, "El jugador 2 ha ganado!!");
       }
    }                                      

    private void boton7ActionPerformed(java.awt.event.ActionEvent evt) {                                       
       if(jugador && movimientos[2][0].equals("i"))
       {
           boton7.setText("X");
           setMovimiento(2,0,"X");
           jugador = false;
           imprimirMatriz();
           if(ganador1())
               javax.swing.JOptionPane.showMessageDialog(null, "El jugador 1 ha ganado!!");
       }
       else if(!jugador && movimientos[2][0].equals("i"))
       {
           boton7.setText("O");
           setMovimiento(2,0,"O");
           jugador = true;
           imprimirMatriz();
           if(ganador2())
               javax.swing.JOptionPane.showMessageDialog(null, "El jugador 2 ha ganado!!");
       }
    }                                      

    private void boton8ActionPerformed(java.awt.event.ActionEvent evt) {                                       
       if(jugador && movimientos[2][1].equals("i"))
       {
           boton8.setText("X");
           setMovimiento(2,1,"X");
           jugador = false;
           imprimirMatriz();
           if(ganador1())
               javax.swing.JOptionPane.showMessageDialog(null, "El jugador 1 ha ganado!!");
       }
       else if(!jugador && movimientos[2][1].equals("i"))
       {
           boton8.setText("O");
           setMovimiento(2,1,"O");
           jugador = true;
           imprimirMatriz();
           if(ganador2())
               javax.swing.JOptionPane.showMessageDialog(null, "El jugador 2 ha ganado!!");
       }
    }                                      

    private void boton9ActionPerformed(java.awt.event.ActionEvent evt) {                                       
         if(jugador && movimientos[2][2].equals("i"))
       {
           boton9.setText("X");
           setMovimiento(2,2,"X");
           jugador = false;
           imprimirMatriz();
           if(ganador1())
               javax.swing.JOptionPane.showMessageDialog(null, "El jugador 1 ha ganado!!");
       }
       else if(!jugador && movimientos[2][2].equals("i"))
       {
           boton9.setText("O");
           setMovimiento(2,2,"O");
           jugador = true;
           imprimirMatriz();
           if(ganador2())
               javax.swing.JOptionPane.showMessageDialog(null, "El jugador 2 ha ganado!!");
       }
    }                                      

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        llenarMatriz();
        boton1.setText("");
        boton2.setText("");
        boton3.setText("");
        boton4.setText("");
        boton5.setText("");
        boton6.setText("");
        boton7.setText("");
        boton8.setText("");
        boton9.setText("");
        jugador = true;
    }                                        

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        System.exit(0);
    }                                        

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {                                         
inicio inicio = new inicio();
inicio.setVisible(true);
this.dispose();        // TODO add your handling code here:
    }                                        

    
    public boolean ganador1()
    {
        if( ((movimientos[0][0].equals("X")) && (movimientos[0][1].equals("X")) && (movimientos[0][2].equals("X")))
                || ((movimientos[1][0].equals("X")) && (movimientos[1][1].equals("X")) && (movimientos[1][2].equals("X")))
                || ((movimientos[2][0].equals("X")) && (movimientos[2][1].equals("X")) && (movimientos[2][2].equals("X")))
                || ((movimientos[0][0].equals("X")) && (movimientos[1][0].equals("X")) && (movimientos[2][0].equals("X")))
                || ((movimientos[0][1].equals("X")) && (movimientos[1][1].equals("X")) && (movimientos[2][1].equals("X")))
                || ((movimientos[0][2].equals("X")) && (movimientos[1][2].equals("X")) && (movimientos[2][2].equals("X")))
                || ((movimientos[0][0].equals("X")) && (movimientos[1][1].equals("X")) && (movimientos[2][2].equals("X")))
                || ((movimientos[0][2].equals("X")) && (movimientos[1][1].equals("X")) && (movimientos[2][0].equals("X"))))
                return true;
                
         else
            return false;
    }
    
    public boolean ganador2()
    {
        if( ((movimientos[0][0].equals("O")) && (movimientos[0][1].equals("O")) && (movimientos[0][2].equals("O")))
                || ((movimientos[1][0].equals("O")) && (movimientos[1][1].equals("O")) && (movimientos[1][2].equals("O")))
                || ((movimientos[2][0].equals("O")) && (movimientos[2][1].equals("O")) && (movimientos[2][2].equals("O")))
                || ((movimientos[0][0].equals("O")) && (movimientos[1][0].equals("O")) && (movimientos[2][0].equals("O")))
                || ((movimientos[0][1].equals("O")) && (movimientos[1][1].equals("O")) && (movimientos[2][1].equals("O")))
                || ((movimientos[0][2].equals("O")) && (movimientos[1][2].equals("O")) && (movimientos[2][2].equals("O")))
                || ((movimientos[0][0].equals("O")) && (movimientos[1][1].equals("O")) && (movimientos[2][2].equals("O")))
                || ((movimientos[0][2].equals("O")) && (movimientos[1][1].equals("O")) && (movimientos[2][0].equals("O"))))
                return true;
                
         else
            return false;
    }
    
    public void setMovimiento(int i, int j, String m)
    {
        movimientos[i][j] = m;
    }
    public void llenarMatriz()
    {
        for(int i = 0; i < 3; i++)
        {
            for(int j = 0; j < 3; j++)
            {
                movimientos[i][j] = "i";
            }
        }
    }
    
    public void imprimirMatriz()
    {
        for(int i = 0; i < 3; i++)
        {
            for(int j = 0; j < 3; j++)
            {
                System.out.print(movimientos[i][j]);
            }
            System.out.print("\n");
        }
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Gato.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Gato.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Gato.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Gato.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Gato().setVisible(true);
            }
        });
    }
    
   
    // Variables declaration - do not modify                     
    private javax.swing.JButton boton1;
    private javax.swing.JButton boton2;
    private javax.swing.JButton boton3;
    private javax.swing.JButton boton4;
    private javax.swing.JButton boton5;
    private javax.swing.JButton boton6;
    private javax.swing.JButton boton7;
    private javax.swing.JButton boton8;
    private javax.swing.JButton boton9;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    // End of variables declaration                   
}
